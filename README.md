# README #


### What is this repository for? ###

A GreyBox of a mountain climber
Ver: 1.19.4

### Elevation/Grid Breakup ###
The mountains are at different elevations and sizes, so that the player can see the largest in the distance, while still manuvering
around the smaller ones.

### Waypointing ###
Mountains progressively get larger, one leading to another, Trying to avoid the "evil tree".

### Who do I talk to? ###
Mark Ribaudo

### Implementations ###

Collectibles- (5 snowmen)
https://learn.unity.com/project/roll-a-ball

MovingPlatforms-
https://www.youtube.com/watch?v=rO19dA2jksk

### Playtesting ###
* Adjusted platforms
* MAde player controller easier to control
