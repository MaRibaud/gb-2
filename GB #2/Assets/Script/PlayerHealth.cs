﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHealth : MonoBehaviour
{
    public static float health;
    public Text healthText;

    private void Start()
    {
        health = 100;
    }

    private void Update()
    {
        healthText.text = " Health " + (int)health;
    }
}
