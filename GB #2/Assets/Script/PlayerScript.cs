﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class PlayerScript : MonoBehaviour
{
	private Rigidbody rb;
	private int count;

	public TextMeshProUGUI countText;
	public GameObject winTextObject;

	void Start()
	{
		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText();

		// Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
		winTextObject.SetActive(false);
	}

	void OnTriggerEnter(Collider other)
	{
		// ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
		if (other.gameObject.CompareTag("Pick Up"))
		{
			other.gameObject.SetActive(false);

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText();
		}
	}

	void SetCountText()
	{
		countText.text = "Count:" + count.ToString();

		if (count >= 5)
		{
			// Set the text value of your 'winText'
			winTextObject.SetActive(true);
		}
	}
}