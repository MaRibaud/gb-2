﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallDamage : MonoBehaviour
{
    public Vector3 enterPos;
    public Vector3 exitPos;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Ground")
        {

            print("enter");

            enterPos = transform.position;
            if (exitPos.y - exitPos.y > 2)
            {
                print("fallingDmg");

                PlayerHealth.health -= 5 * exitPos.y - exitPos.y;
            }
        }

    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Ground")
        {

            print("exit");

            exitPos = transform.position;
        }

    }

}
